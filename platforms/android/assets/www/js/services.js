angular.module('starter.services', ['lbServices'])

.factory('Events', function(Event,$stateParams,$state,$http,BASE_URL, Upload,$ionicLoading,AuthService) {
  // get event status function
  var events = [];
  var requestType = "";// container for request type : admin , user, editor
  var doneEvents = [];
  var futureEvents = [];

  var myDoneEvents = [];
  var myFutureEvents = [];

  function getEventStatus(startTime,endTime,startDate,endDate){
    var date = new Date();
    var parts = date.toString().split(" ");
    var months = {Jan: "01",Feb: "02",Mar: "03",Apr: "04",May: "05",Jun: "06",Jul: "07",Aug: "08",Sep: "09",Oct: "10",Nov: "11",Dec: "12"};
    var todayDate =  parts[3]+"/"+months[parts[1]]+"/"+parts[2];

    var timeNow = new Date();
    var minNow = parseInt(timeNow.getMinutes(),10);
    var hrNow = timeNow.getHours()*60;
    var currTime = (parseInt(hrNow + minNow));

    var time = startTime.split(':'); 
    var startTimeCompare = (+time[0]) * 60 + (+time[1]);
    
    var time = endTime.split(':'); 
    var endTimeCompare = (+time[0]) * 60 + (+time[1]);

    var todayDateCompare = Date.parse(todayDate);
    var startDateCompare = Date.parse(startDate);
    var endDateCompare = Date.parse(endDate);

    if((todayDateCompare == endDateCompare && currTime > endTimeCompare) || todayDateCompare > endDateCompare) status = "Done";
    else if(todayDateCompare == startDateCompare && currTime < startTimeCompare) status = "Today";
    else if(todayDateCompare < startDateCompare) status = "Upcoming";
    else status = "On-going"; 

    return status; // return the status
  }
// function to retrieve user events invited 
  function getMyEvents(userType){
    console.log("sfksjb");
    $ionicLoading.show({ // show loading 
      template: 'Loading...'
    });
    $http.get(BASE_URL.url+'event_users?filter[where][userId]='+AuthService.userID()+'&access_token='+AuthService.accessToken())
    .success(function(data) {
      console.log(data)
     angular.forEach(data, function(value, key) {
      $http.get(BASE_URL.url+'events?filter[where][id]='+value.eventId+'&access_token='+AuthService.accessToken())
      .success(function(newItems) {
        if(newItems[0]!=undefined){
          var status = getEventStatus(newItems[0].start_time,newItems[0].end_time,newItems[0].start_date,newItems[0].end_date)
        var data = { // data for each events - 
          id: newItems[0].id,
          name: newItems[0].name,
          description: newItems[0].description,
          location: newItems[0].location,
          address: newItems[0].address,
          end_date: newItems[0].end_date,
          start_date: newItems[0].start_date,
          end_time: newItems[0].end_time,
          start_time: newItems[0].start_time,
          bitmap:newItems[0].bitmap,
          status: status
        }
        if(status=="Done"){
          myDoneEvents.push(data); // push to myDOne events
        }
        else{
          myFutureEvents.push(data);
        }
        if(userType=='user'){
          events.push(data);// push to events only if user type is user
        }
      }
    })
    });
   })
    .then(function(){
      $ionicLoading.hide(); // close when finished
    })
   }

  if(AuthService.role() == 'admin'){ // if admin request
    $ionicLoading.show({
      template: 'Loading...'
    });
    $http.get(BASE_URL.url+'events?access_token='+AuthService.accessToken())
    .success(function(data) {
     angular.forEach(data, function(value, key) {
    // function call to get the event status
    var status = getEventStatus(value.start_time,value.end_time,value.start_date,value.end_date);
        var data3 = { // data for each events
          id: value.id,
          name: value.name,
          description: value.description,
          location: value.location,
          address: value.address,
          end_date: value.end_date,
          start_date: value.start_date,
          end_time: value.end_time,
          start_time: value.start_time,
          bitmap:value.bitmap,
          status: status,
          created_by: value.created_by
        }
        
        if(status=="Done" && value.created_by==AuthService.userID()){
          doneEvents.push(data3);
        }
        else if(status!="Done" && value.created_by==AuthService.userID()){
          futureEvents.push(data3);
        }
        events.push(data3);
      });
   })    
    .then(function(){
      $ionicLoading.hide();
    })
   getMyEvents(AuthService.role()); // request for data for invited events: function call
    
  }
  else if(AuthService.role() == 'user'){  // if normal user request
    console.log('bsjkgfs');
   getMyEvents(AuthService.role()); // request for data : function call
  }
  return {
    all: function() {
      return events;
    },
    futureEvents: function() {
      return futureEvents;
      
    },
    doneEvents: function() {
      return doneEvents;
    },
    myFutureEvents: function() {
      return myFutureEvents;
      
    },
    myDoneEvents: function() {
      return myDoneEvents;
    },
    addevent: function(array2) { // store to an array ang push it after
      $http.post(BASE_URL.url+'events?access_token='+AuthService.accessToken(),array2)
      .success(function(data) {
        var newEvent = {
          id: data.id,
          name: data.name,
          description: data.description,
          location: data.location,
          address: data.address,
          end_date: data.end_date,
          start_date: data.start_date,
          end_time: data.end_time,
          start_time: data.start_time,
          bitmap:data.bitmap,
          status: "Upcoming"
        }
        futureEvents.push(newEvent); // adds newly created events to the list of future events
        events.push(newEvent); 
        // add timelogs to the db when creating event
        var date = new Date();

        var activity = {
          userName: AuthService.name(),
          eventName: data.name,
          name: "created new event",
          date: date.toString()
        }
        $http.post(BASE_URL.url+'activities?access_token='+AuthService.accessToken(), activity);
      });
      
    }, //added by Mark
        //Upon clicking the update button, the event is updated.
        alterevent: function(event,preBitmap,bitmap) {
         //checks if there is a change in image file before deleting (added by kate)
         if (preBitmap!=bitmap && (preBitmap!=undefined || preBitmap!=null)){
          $http.delete(BASE_URL.url+'/containers/images/files/'+preBitmap+'?access_token='+AuthService.accessToken());
          console.log('deleted!');
        }
        // update the event thru http post with authentications
        $http.post(BASE_URL.url+'events/update?where[id]='+event.id+'&access_token='+AuthService.accessToken(),
        {
          // body request
          name: event.newname,
          description: event.newdescription,
          location: event.location,
          address: event.address,
          start_date: event.newstart_date,
          end_date: event.newend_date,
          start_time: event.newstart_time,
          end_time: event.newend_time,
          bitmap : bitmap
        }
        ).error(function(err){
          console.log(err);
        })

        var date = new Date();

        var activity = {
          userName: AuthService.name(),
          eventName: event.newname,
          name: "updated event details",
          date: date.toString()
        }
        $http.post(BASE_URL.url+'activities?access_token='+AuthService.accessToken(), activity);

      }, 
      remove: function(event) {
        events.splice(events.indexOf(event), 1);
        futureEvents.splice(futureEvents.indexOf(event), 1);
        doneEvents.splice(doneEvents.indexOf(event), 1);
        Event.deleteById({id: event.id}).$promise.then(function() {

          var date = new Date();

          var activity = {
            userName: AuthService.name(),
            eventName: event.name,
            name: "deleted event",
            date: date.toString()
          }

          $http.post(BASE_URL.url+'activities?access_token='+AuthService.accessToken(), activity);
          console.log('deleted');
        });
      }, // deletes done and ongoing/upcoming events

      get: function(id) {
      // console.log(events.length);
      for (var i = 0; i < events.length; i++) {
       console.log(events[i]);
       if (events[i].id === id) {
        return events[i];
      }
    }
    return null;
  },
    // setting the variable to null
    setNull: function(){
      events = [];
      doneEvents = [];
      futureEvents = [];
    }
  };
})


.factory('Users', function(User,$stateParams,$state,$ionicLoading,AuthService) {

  // Might use a resource here that returns a JSON array
  // Some fake testing data
  var users = [];
  
  // console.log(events.length);
  return {
    all: function() {
      return users;
    },
    alteruser: function(user){
      User.update(
      {
        where : 
        {id: AuthService.userID()
        }
      }, {
        name: user.name,
        password: user.pass,
        email: user.email,
        access_token:AuthService.access_token()       // setting the accessToken for Auth
      },function(err, info) {

      });
      window.localStorage.setItem('EMAIL',user.email);
      window.localStorage.setItem('USER_NAME',user.name);
      window.localStorage.setItem('USER_PASS',user.pass);

    },

    remove: function(id) {
       $http.delete(BASE_URL.url+'/events/'+id+'?access_token='+AuthService.accessToken());
    }
  };
})

.factory('EventUsers', function(Event_user, User, $stateParams,$state, $http,BASE_URL,$ionicLoading,AuthService,$ionicPopup) {
  var event_user = Event_user.find({access_token:AuthService.accessToken()});
  console.log(event_user+"pumasok");
  var all = [];
  var individual = [];
  var group = [];
  $ionicLoading.show({
    template: 'Loading...'
  });
  $http.get(BASE_URL.url+'groups?access_token='+AuthService.accessToken()).success(function(data) {
    angular.forEach(data, function(value, key) {
      all.push({
        id: value.id,
        name: value.group_name
      });
      $http.get(BASE_URL.url+'group_users?filter[where][groupId]='+value.id+'&access_token='+AuthService.accessToken()).success(function(data2) {
        var member = [];
        angular.forEach(data2, function(value2, key2) {
          $http.get(BASE_URL.url+'users/'+value2.userId+'?access_token='+AuthService.accessToken()).success(function(data3) {
            member.push({
              member_name: data3.name,
              member_id: data3.id
            });
          })
        })
        group.push({
          id: value.id,
          name: value.group_name,
          members: member
        });
      })
    });
      // $ionicLoading.hide();
    })
  .then(function(){
    $ionicLoading.hide();
  })


  $http.get(BASE_URL.url+'users?access_token='+AuthService.accessToken()).success(function(data) {
    angular.forEach(data, function(value, key) {
      all.push({
        id: value.id,
        name: value.name
      });
      individual.push({
        id: value.id,
        name: value.name,
        selected: 2
      });
    });
  })
  .then(function(){
    $ionicLoading.hide();
  })


  return {
    all: function() {
      return event_user;
    },

    allGroupUser: function() {
      return all;
      
    },

    individual: function() {
      return individual;
    },

    group: function() {
      //console.log(group);
      return group;
    },

    remove: function(id) { // remove user from list of attendees
      $http.delete(BASE_URL.url+'event_users/'+id+'?access_token='+AuthService.accessToken());
    },  

    updateAttendee: function(origAttendee, newAttendee, event_id, event_name) {
      console.log(origAttendee);  
      for(var i = 0; i < origAttendee.length; i++){
       $http.delete(BASE_URL.url+'event_users/'+origAttendee[i].id+'?access_token='+AuthService.accessToken());
       //console.log(origAttendee[i].id+"--"+origAttendee[i].userId);  
     }
     for(var i = 0; i < newAttendee.length; i++){
      $http.post(BASE_URL.url+'event_users?access_token='+AuthService.accessToken(), {'userId':newAttendee[i].userId,'eventId':event_id,'status':newAttendee[i].status});
     }

     var date = new Date();
     var activity = {
      userName: AuthService.name(),
      eventName: event_name,
      name: "updated attendee list",
      date: date.toString()
    }
    $http.post(BASE_URL.url+'activities?access_token='+AuthService.accessToken(), activity);

  },

  //function for check-in functionality (added by kate)
  getEventUser: function(event_id,user_id,distance){
    var eventUser = [];
    //gets the record showing that the user is invited in the event
    $http.get(BASE_URL.url+'event_users?filter[where][eventId]='+event_id+'&filter[where][userId]='+user_id+"&access_token="+AuthService.accessToken())
    .success(function(data) {
      eventUser.push(data[0].id); //gets the id of the record
      eventUser.push(data[0].status); //gets the status of the invitation (invited/checked-in)
      console.log(eventUser);
      console.log(eventUser["0"]);
      console.log(eventUser[1]);

      if (eventUser[1] == "checked-in"){
        //if the user's status of invitation is "checked-in", it will inform the user that he/she already checked-in into the event
          var alertPopup = $ionicPopup.alert({
            title: 'Check-in',
            template: 'You have already checked-in into this event.'
          }); 
      } else {
        //checks if the location of user is within 1km from the event location
          if(distance <= 1000){
              var confirmPopup = $ionicPopup.confirm({
                title: 'Check-in',
                template: 'Are you sure you want to check-in into this event?'
              });

              confirmPopup.then(function(res) {
                //changes the user's status of invitation from "invited" to "checked-in"
                if(res) { 
                  Event_user.update({
                    where : 
                    {id: eventUser["0"]
                    }
                  }, {
                    status : "checked-in"
                  });

                  var alertPopup = $ionicPopup.alert({
                    title: 'Check-in',
                    template: 'You have successfully checked-in into this event.'
                  }); 
                }
              });
          } else {   
              var alertPopup = $ionicPopup.alert({
                title: 'Check-in',
                template: 'Unable to check-in. You are not yet within 1km from event location'
              });        
            }
        }
    })
    return eventUser;
  },

  getAttendee: function (event_id){
    var attendee = [];
    var name;
    $ionicLoading.show({
      template: 'Loading...'
    });
    $http.get(BASE_URL.url+'event_users?filter[where][eventId]='+event_id+'&access_token='+AuthService.accessToken())
    .success(function(data) {
      console.log("success");
     angular.forEach(data, function(value, key) {
      $http.get(BASE_URL.url+'users?filter[where][id]='+value.userId+'&access_token='+AuthService.accessToken())
      .success(function(activity) {
        attendee.push({
          userId: activity[0].id,
          name: activity[0].name,
          status: value.status,
          id: value.id
        })
      })
    })
     
   })
    .then(function(){
      $ionicLoading.hide();
    })
    console.log(attendee);
    return attendee;
  }
};
})

//john.mangcucang end of users
.service('AuthService', function($q, $http,$ionicLoading, USER_ROLES,User,BASE_URL,$ionicPopup) {
  var LOCAL_TOKEN_KEY = 'yourTokenKey';
  var LOCAL_USER_KEY = 'USER_TYPE';
  var username = '';
  var accessToken = '';
  // var userType = [];
  var EMAIL = 'EMAIL';
  var LOCAL_USER_ID = 'USER_ID';
  var isAuthenticated = false;
  var LOCAL_USER_NAME ='USER_NAME';
  var LOCAL_USER_PASS = 'USER_PASS';
  // var role = '';
  var authToken;
  function loadUserCredentials() {
    var token = window.localStorage.getItem(LOCAL_TOKEN_KEY);
    if (token) {
      useCredentials(token);
    }
  }
  function storeUserCredentials(token,id,pw) {
   $ionicLoading.show({ // show loading 
      template: 'Loading...'
    });
    $http.get(BASE_URL.url+'users/'+id+'?access_token='+token)
     .success(function(data) {
       console.log(data);
        // userType['type'] = data.user_type; 
        window.localStorage.setItem(LOCAL_USER_KEY,data.user_type);
        window.localStorage.setItem(LOCAL_USER_NAME,data.name);    
        window.localStorage.setItem(EMAIL,data.email);
      })
      .then(function(){
        $ionicLoading.hide();
      })
    // console.log(userType['type']);
    // set the token , usertype and userID and save to localStorage
    window.localStorage.setItem(LOCAL_USER_PASS,pw); 
    window.localStorage.setItem(LOCAL_TOKEN_KEY,token);
    window.localStorage.setItem(LOCAL_USER_ID,id);
    useCredentials(token);
  }
  function useCredentials(token) {
    isAuthenticated = true;
    authToken = token;
    // Set the token as header for your requests!
    $http.defaults.headers.common['X-Auth-Token'] = token;
  }
  // remove or clean up the localStorage variables and other local variables
  function destroyUserCredentials() {
    authToken = undefined;
    username = '';
    isAuthenticated = false;
    $http.defaults.headers.common['X-Auth-Token'] = undefined;
    window.localStorage.removeItem(LOCAL_TOKEN_KEY);
    window.localStorage.removeItem(LOCAL_USER_KEY);
    window.localStorage.removeItem(LOCAL_USER_ID);
    window.localStorage.removeItem(LOCAL_USER_NAME);
    window.localStorage.removeItem(LOCAL_USER_PASS);
    window.localStorage.removeItem(EMAIL);
  }
  function setUsername(email){
    username = email;
  }
  var login = function(email, pw) {
    return $q(function(resolve, reject) {
      //verify the user credentials
      console.log(email);
      $http.post(BASE_URL.url+'users/login',{"email":email,"password":pw})
      .success(function(data) {
        console.log(data)
           //store the user credentials including the userID
           console.log(pw);
           storeUserCredentials(data.id,data.userId,pw);
           resolve('login success');
      })
      .error(function(data, status) {
        console.log(status);
        if(status== 401){ // if unauthorized login
          reject('Login Failed.');
        }
        else if(status== -1){ // if network error
          var alertPopup = $ionicPopup.alert({
          title: 'Network Error',
          template: 'Check your network connection'
          });
        }
        else{
          var alertPopup = $ionicPopup.alert({
          title: 'Error',
          template: 'Server Error'
          });
        }
      })
    })
  }
  var logout = function() { // destroy credentials when logout
    //logout from server
    $http.post(BASE_URL.url+'users/logout?access_token='+this.accessToken());
    destroyUserCredentials();
  };
  var isAuthorized = function(authorizedRoles) {
    if (!angular.isArray(authorizedRoles)) {
      authorizedRoles = [authorizedRoles];
    }
    return (isAuthenticated && authorizedRoles.indexOf(role) !== -1);
  };
  loadUserCredentials();
  return {
    login: login,
    logout: logout,
    isAuthorized: isAuthorized,
    isAuthenticated: function() {return isAuthenticated;},
    username: function() {return window.localStorage.getItem(EMAIL);}, // return the email
    userID: function() {return  window.localStorage.getItem(LOCAL_USER_ID);}, // return userID
    role: function() {return window.localStorage.getItem(LOCAL_USER_KEY);},// return the role
    name: function() {return window.localStorage.getItem(LOCAL_USER_NAME);}, // return name
    accessToken: function() {return window.localStorage.getItem(LOCAL_TOKEN_KEY);}, // return name
    password: function() {return window.localStorage.getItem(LOCAL_USER_PASS);} //return password
  };
})
.factory('AuthInterceptor', function ($rootScope,$q, AUTH_EVENTS) {
  return {
    responseError: function (response) {
      $rootScope.$broadcast({
        401: AUTH_EVENTS.notAuthenticated,
        403: AUTH_EVENTS.notAuthorized
      }[response.status], response);
      return $q.reject(response);
    }
  };
})
.config(function ($httpProvider) {
  $httpProvider.interceptors.push('AuthInterceptor');
});