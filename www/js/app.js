// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js

// 'use strict';
// var bcrypt = require('bcrypt');
//   const saltRounds = 10;
//   const myPlaintextPassword = 's0/\/\P4$$w0rD';
//   const someOtherPlaintextPassword = 'not_bacon';
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services','ngCordova','ion-floating-menu'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
      console.log("asd");
    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    // var notificationOpenedCallback = function(jsonData) {
    //   console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
    // };

    // window.plugins.OneSignal.startInit("af4e9e38-9362-474e-8df3-39a6b266d6ef").handleNotificationOpened(notificationOpenedCallback).endInit();

    // document.addEventListener("deviceready", onDeviceReady, false);
    // function onDeviceReady() {
    //   console.log("asda");
    //   if(window.plugin != undefined){
    //     var notificationOpenedCallback = function(jsonData) {
    //       alert("Notification opened:\n" + JSON.stringify(jsonData));
    //       console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
    //     };

    //                 // TODO: Update with your OneSignal AppId and googleProjectNumber before running.
    //     window.plugins.OneSignal
    //     .startInit("af4e9e38-9362-474e-8df3-39a6b266d6ef", "557474741189")
    //     .handleNotificationOpened(notificationOpenedCallback)
    //     .endInit();

    // }}


  });
})

.config(function($ionicCloudProvider) {
  $ionicCloudProvider.init({
    "core": {
      "app_id": "18bd5c2d"
    },
    "push": {
      "sender_id": "1054772208932",
      "pluginConfig": {
        "ios": {
          "badge": true,
          "sound": true
        },
        "android": {
          "iconColor": "#343434"
        }
      }
    }
  });
})

.config(function($stateProvider, $urlRouterProvider,USER_ROLES) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
  .state('menu', {
    url: '/menu',
    abstract: true,
    templateUrl: 'templates/menus.html'
  })

  // Each tab has its own nav history stack:

  .state('menu.events', {
    url: '/events',
    views: {
      'menu-content': {
        templateUrl: 'templates/menu-events.html',
        controller: 'EventsCtrl'
      }
    }
  })

  .state('menu.invited-events', {
    url: '/invited-events',
    views: {
      'menu-content': {
        templateUrl: 'templates/menu-invited-events.html',
        controller: 'EventsCtrl'
      }
    }
  })


  .state('menu.create-events',{
    url: '/create-events',
    params: {
      obj: null
    },
    views: {
      'menu-content': {
        templateUrl: 'templates/menu-create-events.html',
        controller: 'CreateEventsCtrl'
      }
    }
    })  // added by Mark and Renz

    //Manage Attendee by Ybur
  .state('menu.events-manage',{
    url: '/events/manage/:id',
    params: {
     obj: null
   },
   views: {
    'menu-content': {
      templateUrl: 'templates/event-manage-attendee.html',
      controller: 'ManageAttendeeCtrl',
      reload: true,
      cache: false

    }
  }
})

  .state('menu.events-update',{
    url: '/events/update/:id',
    views: {
      'menu-content': {
        templateUrl: 'templates/menu-update_events.html',
        controller: 'EventUpdateCtrl'
      }
    }
  })


      //state for event details (added by kate)
      .state('menu.events-details',{
        url: '/events/:id',
        views: {
          'menu-content': {
            templateUrl: 'templates/menu-event-details.html',
            controller: 'EventDetailsCtrl',
            reload: true,
            cache: false
          }
        }
      })

      //state for event details (added by kate)
      .state('menu.invited-events-details',{
        url: '/invited-events/:id',
        views: {
          'menu-content': {
            templateUrl: 'templates/menu-invited-event-details.html',
            controller: 'EventDetailsCtrl'
          }
        }
      })

      //state for generating report
  .state('menu.events-generate',{
    url: '/events/generate/:id',
    params: {
     obj: null
   },
    views: {
      'menu-content': {
        templateUrl: 'templates/event-generate.html',
        controller: 'GenerateReportCtrl'
      }
    }
  }) 

  .state('menu.manage-user',{
    url: '/events/manage/:id/user',
    views: {
      'menu-content': {
        templateUrl: '',
        controller: 'UserListCtrl'
      }
    }
  })
  .state('menu.profile', {
    url: '/profile',
    views: {
      'menu-content': {
        templateUrl: 'templates/menu-profile.html',
        controller: 'ProfileCtrl'
      }
    }
  })

  .state('menu.activity', {
    cache: false,
    url: '/activity',
     views: {
     'menu-content': {
        templateUrl: 'templates/menu-activity.html',
        controller: 'ActivityCtrl'
       }
     }
  })

  .state('menu.edit-profile', {
    url: '/editProfile',
    views: {
      'menu-content': {
       templateUrl: 'templates/menu-edit-profile.html',
       controller: 'editProfileCtrl'
     }
   }

 })



  .state('login', {
    url: '/login',
    templateUrl : 'templates/login.html',
    controller: 'loginCtrl'
  });
  //john.mangcucang end
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise( function($injector, $location) {
    var $state = $injector.get("$state");
    $state.go("menu.invited-events");
  });


})



// .run(function($httpBackend){
//   $httpBackend.whenGET('http://192.168.100.144:3000/api')
//         .respond({message: 'This is my valid response!'});
//   $httpBackend.whenGET('http://localhost:8100/notauthenticated')
//         .respond(401, {message: "Not Authenticated"});
//   $httpBackend.whenGET('http://localhost:8100/notauthorized')
//         .respond(403, {message: "Not Authorized"});

//   $httpBackend.whenGET(/templates\/\w+.*/).passThrough();
//  })

.run(function ($rootScope, $state, AuthService, AUTH_EVENTS) {
  $rootScope.$on('$stateChangeStart', function (event,next, nextParams, fromState) {

    if ('data' in next && 'authorizedRoles' in next.data) {
      var authorizedRoles = next.data.authorizedRoles;
      if (!AuthService.isAuthorized(authorizedRoles)) {
        event.preventDefault();
        $state.go($state.current, {}, {reload: true});
        $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
      }
    }

    if (!AuthService.isAuthenticated()) {
      if (next.name !== 'login') {
        event.preventDefault();
        $state.go('login');
      }
    }
  });
});
