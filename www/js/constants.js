angular.module('starter')

.constant('AUTH_EVENTS', {
	notAuthenticated: 'auth-not-authenticated',
	notAuthorized: 'auth-not-authorized'
})

//base usrl for the api
.constant('BASE_URL',{
	url: 'http://awsevent-mongodbdomain.rhcloud.com/api/'
})

//defined constants for the user roles
.constant('USER_ROLES', {
	admin: true,
	public: false
});
