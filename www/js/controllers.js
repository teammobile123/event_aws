angular.module('starter.controllers', ['ngCordova','ionic-datepicker', 'ionic-timepicker','ngFileUpload','ionic.cloud'])

// main controller for the app
.controller('AppCtrl', function($ionicPush, $ionicHistory,$scope,Events, $state, $ionicPopup, AuthService,$timeout, AUTH_EVENTS,$window,ionicDatePicker, ionicTimePicker) {
  $ionicPush.register().then(function(t) {
    return $ionicPush.saveToken(t);
  }).then(function(t) {
    console.log('Token saved:', t.token);
  });

  $scope.$on('cloud:push:notification', function(event, data) {
    var msg = data.message;
    alert(msg.title + ': ' + msg.text);
  });

  // $scope.admin = AuthService.role();
  if(AuthService.role() == 'admin'){
    console.log(AuthService.role());
    $scope.isAdmin = true;
  }
  $scope.username = AuthService.username();
  $scope.$on(AUTH_EVENTS.notAuthorized, function(event) {
    var alertPopup = $ionicPopup.alert({
      title: 'Unauthorized!',
      template: 'You are not allowed to access this resource.'
    });
  });
if(window.location.pathname=='/'){
 // if currently on login page
//  consoo
}
else{
  $scope.$on(AUTH_EVENTS.notAuthenticated, function(event) {
    AuthService.logout();
    $state.go('login');
    var alertPopup = $ionicPopup.alert({
      title: 'Session Lost!',
      template: 'Sorry, You have to login again.'
    });
  });
}

  $scope.setCurrentUsername = function(name) {
    $scope.username = name;
  };
  $scope.logout = function() {
    var alertPopup = $ionicPopup.show({
     title: 'Log Out',
     template: 'Logging out..'
   });
    $timeout(function() {
     alertPopup.close(); //close the popup after 500 milliseconds for some reason
   }, 500);

    AuthService.logout(); // call logout function
    Events.setNull();
    $state.go('login'); // state go to login
    $timeout(function () {
      $ionicHistory.clearCache();
      $ionicHistory.clearHistory();
    },300)

  };
   // if ( navigator && navigator.splashscreen ) {
   //     navigator.splashscreen.show();
   //      $window.location.reload();
   //      $timeout( function () {
   //      navigator.splashscreen.hide();
   //      }, 500 );
   //   } else {
   //      $window.location.reload();
   //  }

 })

 //Controller for the events
 .controller('EventsCtrl', function($scope,$http,Events,$stateParams,$state,AuthService,$ionicPopup,BASE_URL) {
   $scope.data = {
    showDelete : false
  };
  console.log(AuthService.role()+"lsakjfksj");
    // console.log(AuthService.accessToken());
  $scope.isAdmin = false;
  $scope.isUser = false;
  // if the user is admin then fetch all events
  if(AuthService.role() == 'admin'){
    console.log(AuthService.role()+"lsakjfksj");
    $scope.isAdmin = true;

    //Variable for ng-if in menu-events.html
    $scope.events1 = true;
    $scope.events2 = false;

    $scope.events = Events.futureEvents();
    $scope.doneEvents = Events.doneEvents();

    $scope.myEvents = Events.myFutureEvents();
    $scope.myDoneEvents = Events.myDoneEvents();

    $scope.doRefresh = function() {
      $scope.events = Events.futureEvents();
      $scope.doneEvents = Events.doneEvents();
      // Stop the ion-refresher from spinning
      $scope.$broadcast('scroll.refreshComplete');
    }

    //Select upcoming/on-going events tab in menu-events.html
    $scope.showEvents1 = function(){
      $scope.events1 = true;
      $scope.events2 = false;
    }

    //Select done events tab in menu-events.html
    $scope.showEvents2 = function(){
      $scope.events1 = false;
      $scope.events2 = true;
    }
      // when the add button is click go to
      $scope.addEvents = function(){
        $state.go('menu.create-events', {}, {reload: true});
      }

      $scope.onItemDelete = function(event) {
        var confirmPopup = $ionicPopup.confirm({
          title: 'Delete Event',
          template: 'Are you sure you want to delete this event?'
        });
        confirmPopup.then(function(res) {
          if(res) {
            Events.remove(event);
          } else{
            console.log('Delete operation cancelled');
          }
        });
      }
    }
// if the user is normal user
else if(AuthService.role() == 'user'){
   $scope.isUser = true; // set the flag to true
    //Variable for ng-if in menu-events.html
    $scope.events1 = true;
    $scope.events2 = false;
    $scope.myEvents = Events.myFutureEvents();
    $scope.myDoneEvents = Events.myDoneEvents();
    console.log($scope.myEvents);


    $scope.doRefresh = function() {
      $scope.myEvents = Events.myFutureEvents();
      $scope.myDoneEvents = Events.myDoneEvents();
      // Stop the ion-refresher from spinning
      $scope.$broadcast('scroll.refreshComplete');
    }
    //Select upcoming/on-going events tab in menu-events.html
    $scope.showEvents1 = function(){
      $scope.events1 = true;
      $scope.events2 = false;
    }

    //Select done events tab in menu-events.html
    $scope.showEvents2 = function(){
      $scope.events1 = false;
      $scope.events2 = true;
    }
  }
})
//controller for create event
.controller('TimeDatePickerCtrl', function($scope, ionicDatePicker, ionicTimePicker) {
  $scope.timeValidation = function(){
    var startDate = document.getElementById("datestart").value;
    var startDateCompare = Date.parse(startDate);
    var endDate = document.getElementById("dateend").value;
    var endDateCompare = Date.parse(endDate);
    var startTime = document.getElementById("timestart").value;
    var time = startTime.split(':');
    var startTimeCompare = (+time[0]) * 60 + (+time[1]);
    var endTime = document.getElementById("timeend").value;
    var time = endTime.split(':');
    var endTimeCompare = (+time[0]) * 60 + (+time[1]);

    var timeNow = new Date();
    var minNow = parseInt(timeNow.getMinutes(),10);
    var hrNow = timeNow.getHours()*60;
    var currTime = (parseInt(hrNow + minNow));
    console.log("TIME NOW "+currTime);
    console.log("Start TIME "+startTimeCompare);
    console.log("START DATE "+startDateCompare);
    console.log("NOW DATE "+timeNow.toString());

    var date = new Date();
    var parts = date.toString().split(" ");
    var months = {Jan: "01",Feb: "02",Mar: "03",Apr: "04",May: "05",Jun: "06",Jul: "07",Aug: "08",Sep: "09",Oct: "10",Nov: "11",Dec: "12"};
    var todayDate =  parts[3]+"/"+months[parts[1]]+"/"+parts[2];
    var todayDateCompare = Date.parse(todayDate);
    console.log("NOW DATE2 "+todayDateCompare);

    if((startDateCompare==todayDateCompare)&&(startDateCompare==endDateCompare)){
     if ((startTimeCompare > currTime) && (startTimeCompare < endTimeCompare)) {
      $scope.myForm.timeend.$setValidity("timeEndError", true);
      $scope.myForm.timeend.$setDirty();
    }
    else{
      $scope.myForm.timeend.$setValidity("timeEndError", false);
      $scope.myForm.timeend.$setDirty();
    }
  } else if((startDateCompare==todayDateCompare)&&(startDateCompare<endDateCompare)){
    if (startTimeCompare > currTime) {
      $scope.myForm.timeend.$setValidity("timeEndError", true);
      $scope.myForm.timeend.$setDirty();
    }
    else{
     $scope.myForm.timeend.$setValidity("timeEndError", false);
     $scope.myForm.timeend.$setDirty();
   }
 }
 else if((startDateCompare>todayDateCompare)&&(startDateCompare==endDateCompare)){
  if (startTimeCompare < endTimeCompare) {
    $scope.myForm.timeend.$setValidity("timeEndError", true);
    $scope.myForm.timeend.$setDirty();
  }
  else{
   $scope.myForm.timeend.$setValidity("timeEndError", false);
   $scope.myForm.timeend.$setDirty();
 }
} else if((startDateCompare>todayDateCompare)&&(startDateCompare<endDateCompare)){
 $scope.myForm.timeend.$setValidity("timeEndError", true);
 $scope.myForm.timeend.$setDirty();
} else if(startDateCompare<endDateCompare){
  $scope.myForm.timeend.$setValidity("timeEndError", true);
  $scope.myForm.timeend.$setDirty();
}else{
 $scope.myForm.timeend.$setValidity("timeEndError", false);
 $scope.myForm.timeend.$setDirty();
}
}
 //added by Renz
 //time validation

 var datestart = {
      callback: function (val) {  //Mandatory
        console.log('Return value from the datepicker popup is : ' + val, new Date(val));
        //Convert date to YYYY/MM/DD format
        var date = new Date(val).toString();
        var parts = date.split(" ");
        var months = {Jan: "01",Feb: "02",Mar: "03",Apr: "04",May: "05",Jun: "06",Jul: "07",Aug: "08",Sep: "09",Oct: "10",Nov: "11",Dec: "12"};
        var startDate =  parts[3]+"/"+months[parts[1]]+"/"+parts[2];

        document.getElementById("datestart").value = startDate;
        //Get current date YYYY/MM/DD format
        var date = new Date().toString();
        var parts = date.split(" ");
        var months = {Jan: "01",Feb: "02",Mar: "03",Apr: "04",May: "05",Jun: "06",Jul: "07",Aug: "08",Sep: "09",Oct: "10",Nov: "11",Dec: "12"};
        var todayDate =  parts[3]+"/"+months[parts[1]]+"/"+parts[2];
        var todayDateCompare = Date.parse(todayDate);
        var startDateCompare = Date.parse(startDate);
        //Enable select time button when start date is set
        document.getElementById("timestartbutton").removeAttribute('disabled');
        document.getElementById("timeendbutton").removeAttribute('disabled');
        //Start Date Validation if Start Date is less than Current Date show error
        if (startDateCompare >= todayDateCompare) {
          $scope.myForm.datestart.$setValidity("dateStartError", true);
          $scope.myForm.datestart.$setDirty();
        } else {
          $scope.myForm.datestart.$setValidity("dateStartError", false);
          $scope.myForm.datestart.$setDirty();
        }
        //If End Date is Set and Start Date is changed
        var endDate = document.getElementById("dateend").value;
        if(endDate){
          var endDateCompare = Date.parse(endDate);
          if (endDateCompare >= startDateCompare) {
            $scope.myForm.dateend.$setValidity("dateEndError", true);
            $scope.myForm.dateend.$setDirty();
          } else {
            $scope.myForm.dateend.$setValidity("dateEndError", false);
            $scope.myForm.dateend.$setDirty();
          }
        }
        var endTime = document.getElementById("timeend").value;
        if(endTime) $scope.timeValidation();
        $scope.event.start_date = startDate;
        $scope.event.newstart_date = $scope.event.start_date;
      }
    };
    var dateend = {
      callback: function (val) {  //Mandatory
        console.log('Return value from the datepicker popup is : ' + val, new Date(val));
        var date = new Date(val).toString();
        var parts = date.split(" ");
        var months = {Jan: "01",Feb: "02",Mar: "03",Apr: "04",May: "05",Jun: "06",Jul: "07",Aug: "08",Sep: "09",Oct: "10",Nov: "11",Dec: "12"};
        var endDate =  parts[3]+"/"+months[parts[1]]+"/"+parts[2];
        document.getElementById("dateend").value = endDate;
        var endDateCompare = Date.parse(endDate);
        var startDate = document.getElementById("datestart").value;
        var startDateCompare = Date.parse(startDate);
        //End Date Validation if End Date is less than Start Date show error
        if (endDateCompare >= startDateCompare) {
          $scope.myForm.dateend.$setValidity("dateEndError", true);
          $scope.myForm.dateend.$setDirty();
        } else {
          $scope.myForm.dateend.$setValidity("dateEndError", false);
          $scope.myForm.dateend.$setDirty();
        }
        var endTime = document.getElementById("timeend").value;
        if(endTime) $scope.timeValidation();
        $scope.event.end_date = endDate;
        $scope.event.newend_date = $scope.event.end_date;
      }
    };
    $scope.openDatePicker1 = function(){
      ionicDatePicker.openDatePicker(datestart);
    }
    $scope.openDatePicker2 = function(){
      ionicDatePicker.openDatePicker(dateend);
    }
    $scope.submit = function() {
      $scope.myForm.$setPristine();
    }
    var timestart = {
      callback: function (val) {      //Mandatory
        if (typeof (val) === 'undefined') {
          console.log('Time not selected');
        }
        else {
          var selectedTime = new Date(val * 1000);
          console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), 'H :', selectedTime.getUTCMinutes(), 'M');
          console.log(selectedTime.getUTCMinutes().toString().length);
          if(selectedTime.getUTCMinutes().toString().length==1){
            var minutes = "0"+selectedTime.getUTCMinutes();
          }
          else var minutes = selectedTime.getUTCMinutes();
         // document.getElementById("timestart").value = selectedTime.getUTCHours()+":"+minutes;
          //Time validation

          document.getElementById("timestart").value = selectedTime.getUTCHours()+":"+minutes;
          $scope.event.start_time = selectedTime.getUTCHours()+":"+minutes;
          $scope.event.newstart_time = $scope.event.start_time;

          $scope.timeValidation();
        }
      },
      step: 1
    };
    var timeend = {
      callback: function (val) {      //Mandatory
        if (typeof (val) === 'undefined') {
          console.log('Time not selected');
        }
        else {
          var selectedTime = new Date(val * 1000);
          console.log('Selected epoch is : ', val, 'and the time is ', selectedTime.getUTCHours(), 'H :', selectedTime.getUTCMinutes(), 'M');

          if(selectedTime.getUTCMinutes().toString().length==1){
            var minutes = "0"+selectedTime.getUTCMinutes();
          }
          else var minutes = selectedTime.getUTCMinutes();
          //Time validation
          document.getElementById("timeend").value = selectedTime.getUTCHours()+":"+minutes;
          $scope.event.end_time = selectedTime.getUTCHours()+":"+minutes;
          $scope.event.newend_time =  $scope.event.end_time;

          $scope.timeValidation();
        }
      },
      step: 1
    };

    $scope.openTimePicker1 = function(){
      ionicTimePicker.openTimePicker(timestart);
    }
    $scope.openTimePicker2 = function(){
      ionicTimePicker.openTimePicker(timeend);
    }
  })

  //controller for upload image (added by kate)
  .controller('UploadCtrl', function ($scope, Upload, $timeout, $rootScope,$ionicPopup,$ionicLoading,$http,BASE_URL,Events,AuthService){
    var dateStamp = new Date();
    var filename = dateStamp.getTime();

    $scope.uploadPic = function(file){
      console.log(file);
      $rootScope.ImageFilename = filename + ".jpg"; //image filename that will be saved in the database
      console.log("filename: "+$rootScope.ImageFilename);
      file = Upload.rename(file,$rootScope.ImageFilename);
      file.upload = Upload.upload({
          url: 'http://awsevent-mongodbdomain.rhcloud.com/api/containers/images/upload?access_token='+AuthService.accessToken(), //url of the database
          data: {file: file},
        });

      file.upload.then(function (response) {
        $ionicLoading.hide();
        $timeout(function () {
          file.result = response.data;
        });
      },

      function (response) {
        if (response.status > 0)
          $scope.errorMsg = response.status + ': ' + response.data;
      },

      function (evt) {
        $ionicLoading.show({
          template: 'Uploading...'
        });
      });
    }
  })

.controller('ProfileCtrl', function ($scope, AuthService,$ionicModal,Users,$ionicPopup,$ionicPopover,$ionicLoading){
  $scope.modl = {
    email : AuthService.username(),
    name : AuthService.name()
  }
  $ionicModal.fromTemplateUrl('templates/profile.html', { // load the template
     scope: $scope,                                     // which contains the div for map
     animation: 'slide-in-up'
  }).then(function(modal) {
      $scope.modal = modal;
  });
  $scope.openModal = function(user1) { // open modal
      $scope.modal.show();
      $scope.user = {
       email : AuthService.username(),
       pass : AuthService.password(),
       name : AuthService.name()
      }
  }
   //close and reset modal
  $scope.closeModal= function() {
      $scope.modal.hide(); 
  }
  $scope.saveProfile = function(user1){
      var confirmPopup = $ionicPopup.confirm({
      title: 'Save',
      template: 'Are you sure you want to update your profile?'
      });
      confirmPopup.then(function(res) {
        if(res) { //confirmation of the ionic Popup
          Users.alteruser(user1);   
          $scope.modl.email = $scope.user.email;
          $scope.modl.name = $scope.user.name;  
          console.log(user1); 
          $ionicLoading.show({
          template: 'Profile Saved!',
          duration: 3000
          }).then(function(){
          ;});
                                
        } else {
          // cancel ionic popUp
          console.log('Cancelled');
        } 
        $scope.modal.hide();
    });
  }
  $scope.showPopup = function(user1) { //load show popUp
    var dbOldPass = AuthService.password();
    var dataPass = "";
    var dataNewPass = "";
    var dataConfirmPass = "";
  
    $scope.pass = {};
    $scope.response = false;
    var showPopup = $ionicPopup.show({
      title: 'Change Password',
      template: '<label class="item item-input"><input type="password" id="oldPass" ng-model="pass.password" placeholder=" Old Password"></label>' + 
                '<label class="item item-input"><input type="password" id="newPass" ng-model="pass.newPassword" placeholder=" New Password"></label>' +
                '<label class="item item-input"><input type="password" id="confirmPass" ng-model="pass.confirmPassword" placeholder=" Confirm Password"></label>',
      scope: $scope,
      buttons: [
        { text: 'Cancel' },
        {
          text: '<b>Save</b>',
          type: 'button-positive',
           onTap: function(e) {   
            if (!$scope.pass.password){
             var alertPopup = $ionicPopup.alert({
             title: 'Error!',
             template: 'Password required. Please try again.'
             });
             alertPopup.then(function(res) {
             console.log('password required!');
            });
              e.preventDefault();
              console.log("Popup here");
            } else {
              dataPass = $scope.pass.password;
              dataNewPass = $scope.pass.newPassword;
              dataConfirmPass = $scope.pass.confirmPassword;
              $scope.response = true;
              return $scope.response;
            }
            
           }
        }
      ]
    });
    showPopup.then(function(res){
  
    // console.log(dataPass + dataNewPass + dataConfirmPass);
 
    if (res) {
        
      if (dataPass == dbOldPass) { // validate if input password match the old password
            if (dataNewPass != dataConfirmPass) {
                                  var alertPopup = $ionicPopup.alert({
                                  title: 'Invalid!',
                                  template: 'Password do not match. Please try again.'
                                  });
                                  alertPopup.then(function(res) {
                                  console.log('Retry!');
                                  });
            } else if (dataNewPass && dataConfirmPass != "") {
                                  $scope.user.pass = dataNewPass;
                                  Users.alteruser(user1);
                                  // $scope.modl.email = $scope.user.email;
                                  // $scope.modl.name = $scope.user.name;
                                  $ionicLoading.show({
                                  template: 'Password Saved!',
                                  duration: 3000
                                  }).then(function(){
                                  ;});
                                  console.log(user1);
            } else {
                                  var alertPopup = $ionicPopup.alert({
                                  title: 'Error!',
                                  template: 'Password required. Please try again.'
                                  });
                                  alertPopup.then(function(res) {
                                  console.log('password required!');
                                  });
                                  }
        } else if ((dataConfirmPass == "") && (dataPass == "") && (dataNewPass == "")) { //validate if password is required
            var alertPopup = $ionicPopup.alert({
            title: 'Error!',
            template: 'Password required. Please try again.'
            });
            alertPopup.then(function(res) {
            console.log('password required!');
            });
        } else {
            var alertPopup = $ionicPopup.alert({ // validate if password is incorrect
            title: 'Error!',
            template: 'Password incorrect. Please try again.'
            });
            alertPopup.then(function(res) {
            console.log('Retry!');
            });
            }
        // document.getElementById("oldPass").value = "";  //clear the textbox after saving
        // document.getElementById("newPass").value = "";
        // document.getElementById("confirmPass").value = "";  
      console.log("Save");
      
    } else {
       //console.log(dataPass + dataNewPass + dataConfirmPass);
      console.log("Cancel");
    }
  });
}
})

  //controller for event details (added by kate)
  .controller('EventDetailsCtrl', function($scope,AuthService, $stateParams,Events, $ionicModal,$timeout) {
    $scope.token = AuthService.accessToken();
    if(AuthService.role() == 'admin'){ // check if the user is admin
      $scope.isAdmin=true;             // set isAdmin  to true
    }
    else if(AuthService.role() == 'user') { // check if the user is a normal user
      $scope.isUser = true;                 // set isUser to true
    }
    $scope.event = Events.get($stateParams.id);
    $ionicModal.fromTemplateUrl('templates/map3.html', { // load the template
      scope: $scope,                                     // which contains the div for map
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });

    // function for clicking the show map location
    $scope.openModal = function(location,eventId) {
          $scope.modal.show(); // show the modal
          var events = Events.all(); // store all the events
          var eventLocation = location;
          // initialized a map variable with options
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 13,
            noClear: true,
            mapTypeId: 'roadmap',
            disableDefaultUI: true
          });
          var myLatLng = {
            lat: parseFloat((eventLocation.split(',')[0])),
            lng: parseFloat((eventLocation.split(',')[1]))
          };
          // loop other events
          var markers = []; // variable array for markers
          var contentString = []; // variable array for markers
          var infowindow = []; // variable container for infowindow
          for (var i = 0; i < events.length; i++) { // loop thru the events
            var eventLocation = events[i].location;  // store it to a eventLocation variable
            // create a latlang object for each event to be use in displaying the map
            var LatLng = {
              lat: parseFloat((eventLocation.split(',')[0])),
              lng: parseFloat((eventLocation.split(',')[1]))
            };
            var distance = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(myLatLng), new google.maps.LatLng(LatLng)); // get the distance from event
             // display only the nearest events within 1km
             if (distance < 1000 && distance > 0  || events[i].id == eventId) {
                  // create a event marker
                  markers[i] = new google.maps.Marker({
                    position: LatLng,
                    map: map,
                    title: events[i].name
                  });
                  // create a window info for an events
                  contentString[i] = '<div>' +
                  '<h3 style="color:blue;text-align:center;"><img style = "width:25px;length:25px" src="http://awsevent-mongodbdomain.rhcloud.com/api/containers/images/download/'+events[i].bitmap+'?access_token='+$scope.token+'"> &nbsp;' + events[i].name + '</h3>' +
                  '<div id="bodyContent' + i + '">' +
                  '<ul>' +
                  '<li> <b>Date : </b>' + events[i].start_date + ' to ' + events[i].end_date + '<li>' +
                  '<li> <b>Time : </b>' + events[i].start_time + ' to ' + events[i].end_time + '<li>' +
                  '<li> <b>Address : </b>' + events[i].address + '<li>' +
                  '<li> <b>Description : </b>' + events[i].description + '<li>' +
                  '</ul>' +
                  '</br><p style="text-align:center;"> <a href="https://maps.google.com/maps?ll=' + (eventLocation.split(',')[0]) + ',' + parseFloat((eventLocation.split(',')[1])) + '&z=17&t=m&hl=en-US&gl=US&mapclient=apiv3&cid=8413930365324387885" target="_blank">view in google maps</a></p>' +
                  '</div>' +
                  '</div>';
                  // create a unique info windows
                  infowindow[i] = new google.maps.InfoWindow({
                    content: contentString[i]
                  });
                  //trigger the info window of the current event
                  if (events[i].id == eventId) {
                    map.setCenter(myLatLng);
                    infowindow[i].open(map, markers[i]);
                  }
                  // trigger the info window on clicks
                  google.maps.event.addListener(markers[i], 'click', function(i) {
                    return function() {
                      infowindow[i].open(map, markers[i]);
                    }
                  }(i));
                }
              }
            };
            $scope.closeModal = function() {
              $scope.modal.hide();
            };
            //Remove modal
            $scope.$on('$destroy', function () {
              $scope.modal.remove();
            });

              //Set $scope.map to null
              $scope.$on('modal.hidden', function () {
                $scope.$on('$destroy', function () {
                  $scope.map = null;
                });
              });
            })

  //RENZ1
  //Manage Attendee Controller by Ybur
  .controller('ManageAttendeeCtrl', function($scope,$ionicHistory,$state, $stateParams, Events, Users, EventUsers, $ionicModal, $ionicPopup) {
    $scope.event = Events.get($stateParams.id);
    $scope.newAttendee = [];
    $scope.origAttendee = [];
    $scope.newAttendee = EventUsers.getAttendee($scope.event.id);
    $scope.origAttendee = EventUsers.getAttendee($scope.event.id);
    $scope.individual = true;
    $scope.group = false;
    console.log($scope.origAttendee);

    $ionicModal.fromTemplateUrl('my-modal.html', {
      scope: $scope,
      animation: 'slide-in-up'
    }).then(function(modal) {
      $scope.modal = modal;
    });

    $scope.openModal = function() {
     // $scope.usersAll = Users.all();

     $scope.usersAll = EventUsers.allGroupUser();
     $scope.usersIndividualAll = EventUsers.individual();
     $scope.usersGroupAll = EventUsers.group();

     $scope.usersIndividual = [];
     $scope.usersGroup = [];

     for(var i = 0; i < $scope.usersIndividualAll.length; i++){
      var found = 0;
      for(var j = 0; j < $scope.newAttendee.length; j++){
        if($scope.usersIndividualAll[i].id==$scope.newAttendee[j].userId){
          found = 1;
          break;
        }
      }
      if(found==0) $scope.usersIndividual.push($scope.usersIndividualAll[i]);
    }

      //console.log($scope.usersGroup);

      for(var i = 0; i < $scope.usersGroupAll.length; i++){
        var members = [];
        for(var j = 0; j < $scope.usersGroupAll[i].members.length; j++){
          var found = 0;
          //console.log($scope.usersGroupAll[i].name+"-"+$scope.usersGroupAll[i].members[j].member_name);
          for(var k = 0; k < $scope.newAttendee.length; k++){
            if($scope.usersGroupAll[i].members[j].member_id==$scope.newAttendee[k].userId){
              found = 1;
              break;
            } 
          }
          if(found==0) members.push($scope.usersGroupAll[i].members[j]);
        }

        $scope.usersGroup.push({
          id: $scope.usersGroupAll[i].id,
          members: members,
          name: $scope.usersGroupAll[i].name
        })
      }

      // console.log($scope.newAttendee);
      // console.log("alluser");
      // console.log($scope.usersIndividualAll);
      // //console.log("START");

      // for(var j = 0; j < $scope.newAttendee.length; j++){
      //   //console.log($scope.newAttendee[j].name);
      //   var tempAttendee = [];
      //   tempAttendee.push({
      //     id: $scope.newAttendee[j].userId,
      //     name: $scope.newAttendee[j].name,
      //     selected: 1
      //   }); 
      //  // $scope.usersIndividual.splice(JSON.parse(angular.toJson($scope.usersIndividual)).indexOf(tempAttendee), 1);
      //   console.log(tempAttendee);
      //   console.log(JSON.parse(angular.toJson($scope.usersIndividual)));
      //   console.log(tempAttendee[0].name+"/"+JSON.parse(angular.toJson($scope.usersIndividual)).indexOf($scope.newAttendee[j]))
      // }

      // console.log("/////");
      // console.log($scope.usersIndividual);
      // console.log(tempAttendee);
      // console.log("/////");

      //$scope.usersIndividual = $scope.usersIndividualAll;

      $scope.modal.show();

    };

    $scope.closeModal = function() {
      $scope.modal.hide();
    };

    $scope.updateAttendee = function(event_id, event_name) {
      EventUsers.updateAttendee($scope.origAttendee, $scope.newAttendee, event_id, event_name);

      var confirmPopup = $ionicPopup.alert({
        title: 'Update Attendee',
        template: 'Attendee list updated'
      });
      $ionicHistory.goBack();
      // $state.transitionTo($state.current, angular.copy($stateParams), { reload: true, inherit: true, notify: true });
       //$state.go('menu.events-details', {id: event_id},{ reload: true, inherit: true, notify: true });
     };

     $scope.getAttendee = function(event_id) {
      EventUsers.getAttendee(event_id);
    };

    $scope.addToList = function(id, name){
      $scope.newAttendee.push({userId:id, name:name, status:'Invited'});
      $scope.closeModal();
    }

    $scope.selectFilter = function (filter) {
      if(filter=='individual'){
        $scope.individual = true;
        $scope.group = false;
        $scope.v = false;
      }
      else if(filter=='group'){
        $scope.individual = false;
        $scope.group = true;
        $scope.v = false;
      }

    };

    $scope.checkAll = function (users) {
      if($scope.v==true){
        $scope.v = false;
        for(var i in users){
          users[i].selected = 2;
        }
      } 
      else{
        $scope.v = true;
        for(var i in users){
          users[i].selected = 1;
        }
      }
    };

    $scope.checkAll2 = function (users) {
      if($scope.v==true){
        $scope.v = false;
        for(var i in users){
          for(var j = 0; j < users[i].members.length; j++){
            users[i].members[j].selected = 2;
          }
        }
      } 
      else{
        $scope.v = true;
        for(var i in users){
          for(var j = 0; j < users[i].members.length; j++){
            users[i].members[j].selected = 1;
          }
        }
      }
    };

    $scope.checkAll3 = function (name, id, users) {
      if(id==false){
        $scope.v[id] = false;
        for(var i in users){
          if(name == users[i].name){
            for(var j = 0; j < users[i].members.length; j++){
              users[i].members[j].selected = 2;
            }
            break;
          }
        }
      } 
      else{
        $scope.v[id] = true;
        for(var i in users){
          if(name == users[i].name){
            for(var j = 0; j < users[i].members.length; j++){
              users[i].members[j].selected = 1;
            }
            break;
          }
        }
      }
    };

    $scope.check= function(data) { 
      var arr = [];
      for(var i in data){
        //console.log(data[i].name+"-"+data[i].selected);
        if(data[i].selected=='1'){
          $scope.newAttendee.push({userId:data[i].id, name:data[i].name, status:'Invited'});
        }
      }
      //console.log($scope.newAttendee);
      $scope.closeModal();
    }

    $scope.check2= function(data) { 
      var arr = [];

      for(var i = 0; i < data.length; i++){
        // console.log(data[i]);
        for(var j = 0; j < data[i].members.length; j++){
          if(data[i].members[j].selected=='1'){

            var found = 0;
            for(var k = 0; k < $scope.newAttendee.length; k++){

              if($scope.newAttendee[k].userId==data[i].members[j].member_id){
                found = 1;
                break;
              }
            }
            if(found==0) $scope.newAttendee.push({userId:data[i].members[j].member_id, name:data[i].members[j].member_name, status:'Invited'});
          }
        }
      }
      $scope.closeModal();
    } 

    $scope.toggleGroup = function(group) {
      if ($scope.isGroupShown(group)) {
        $scope.shownGroup = null;
      } else {
        $scope.shownGroup = group;
      }
    };

    $scope.isGroupShown = function(group) {
      return $scope.shownGroup === group;
    };

    $scope.removeAttendee = function(id){


      var confirmPopup = $ionicPopup.confirm({
        title: 'Remove',
        template: 'Are you sure you want to remove this attendee?'

      });

      confirmPopup.then(function(res) {
        if(res) { 
         for(var j = 0; j < $scope.newAttendee.length; j++){
          if($scope.newAttendee[j].userId==id){
            $scope.newAttendee.splice(j, 1);
          }
        }
        $state.transitionTo($state.current, angular.copy($stateParams), { reload: true, inherit: true, notify: true });
                //$state.go('menu.events', { reload: true, inherit: true, notify: true });
              }
            });
    }

  })
/**Controller for Generating Report
    Added by Mark and Renz
**/
.controller('GenerateReportCtrl', function($scope,$state,$stateParams, Events, Users, EventUsers, $ionicModal, $ionicPopup){
  
  $scope.event = Events.get($stateParams.id);
   $scope.origAttendee = EventUsers.getAttendee($scope.event.id);
   $scope.usersGroupAll = EventUsers.group();
   $scope.invitedGroups = [];

   var confirmPopup = $ionicPopup.confirm({
      title: 'Generate Report',
      template: 'Do you want to genrate report?'

   });

   confirmPopup.then(function(res) {
      if(res) {
        for(var i = 0; i < $scope.usersGroupAll.length; i++){
          var members = [];
          var found = 0;
          for(var j = 0; j < $scope.usersGroupAll[i].members.length; j++){
          //console.log($scope.usersGroupAll[i].name+"-"+$scope.usersGroupAll[i].members[j].member_name);
          for(var k = 0; k < $scope.origAttendee.length; k++){
            if($scope.usersGroupAll[i].members[j].member_id==$scope.origAttendee[k].userId){
              found++;
              break;
            }
          }
          if(found==$scope.usersGroupAll[i].members.length) {
            $scope.invitedGroups.push({
              id: $scope.usersGroupAll[i].id,
              name: $scope.usersGroupAll[i].name
            })
          }
        }
      }
      } else location.href='menu-events';
    });
  


   


//   console.log("size of group is: "+$scope.usersGroupAll.length);
   

  //  for(var i = 0; i < $scope.usersGroupAll.length; i++){
  //   for(var j = 0; j < $scope.usersGroupAll[i].members.length; j++){
        
  //   }
  // }
})


.controller('EventUpdateCtrl', function($scope,$state,$ionicNavBarDelegate, $stateParams,$ionicPopup, Events,$ionicModal,$compile,$timeout,$rootScope)  {
  $rootScope.askConfirm = function() {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Go Back',
      template: 'You are going back to the previous screen.'

    });

    confirmPopup.then(function(res) {
      if(res) { 
        if($rootScope.isNative){
          location.href='menu-events';
        }else{
          $ionicNavBarDelegate.back();
        } 
      }
    });
  }

  $scope.event = Events.get($stateParams.id);
      var marker = new google.maps.Marker({}); // initialize event marker
      $ionicModal.fromTemplateUrl('templates/map.html', {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function(modal) {
        $scope.modal = modal;
      });

      console.log($scope.event)
      var count = 0;
      $scope.editLocation = function(location, address) {
          $scope.modal.show(); // shows the modal
          // if the modal is open for the first time
          if (count == 0) {
            console.log();
            var loc;
            var map = new google.maps.Map(document.getElementById('map_canvas1'), {
              zoom: 15,
              noClear: true,
              center: { lat: parseFloat(location.split(',')[0]), lng: parseFloat(location.split(',')[1]) },
              mapTypeId: 'roadmap',
              disableDefaultUI: true
            });
              contentString1 = "<div><h4>" + address + "</h4><br><button class='button button-clear button-positive' ng-click='setLocation()' >Set as new event location</button></div>"; //complile the content
              var compiled = $compile(contentString1)($scope); //complile the content
              infowindow1 = new google.maps.InfoWindow({
                content: compiled[0]
              });
              marker = new google.maps.Marker({
                position: { lat: parseFloat(location.split(',')[0]), lng: parseFloat(location.split(',')[1]) },
                map: map,
                zoom: 5,
                draggable: true,
                title: address
              });
              infowindow1.open(map, marker);
              // add eventclick listener to listen on click toggle the infowindow
              marker.addListener('click', function() {
                infowindow1.open(map, marker);
              });
              //Create the search box and link it to the UI element.
              document.getElementById('pac').value = address;
              var input = document.getElementById('pac');
              var searchBox = new google.maps.places.SearchBox(input);
              map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
              // Bias the SearchBox results towards current map's viewport.
              map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
              });
              // Listen for the event fired when the user selects a prediction and retrieve
              // more details for that place.
              searchBox.addListener('places_changed', function() {
                marker.setMap(null);
                var places = searchBox.getPlaces();
                if (places.length == 0) {
                  return;
                }
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                 if (!place.geometry) {
                   console.log("Returned place contains no geometry");
                   return;
                 }
                 loc = place.geometry.location;
                 if (place.geometry.viewport) {
                         // Only geocodes have viewport.
                         bounds.union(place.geometry.viewport);
                       } else {
                         bounds.extend(place.geometry.location);
                       }
                     });
                map.fitBounds(bounds);
                  // var geocoder = new google.maps.Geocoder;
                  address = document.getElementById("pac").value;
                  marker = new google.maps.Marker({
                    position: loc,
                    map: map,
                    zoom: 10,
                    draggable: true,
                    title: address
                  });
                  contentString = "<div><h4>" + address + "</h4><br><button class='button button-clear button-positive' ng-click='setLocation()'>Set as new event location</button></div>";
                  var compiled = $compile(contentString)($scope); //complile the content
                  infowindow = new google.maps.InfoWindow({
                    content: compiled[0]
                  });
                    // add eventclick listener to listen on click toggle the infowindow
                    infowindow1.close(map, marker);
                    infowindow.open(map, marker);
                    marker.addListener('click', function() {
                      infowindow.open(map, marker);
                    });
                  });


              $scope.setLocation = function() {
                location = marker.getPosition().lat().toString() + "," + marker.getPosition().lng().toString();
                $scope.event.address = address;
                $scope.event.location = location;
                console.log(location);
                $scope.closeModal();
              }
            }
          count = count + 1; // increment the modal if its open once
        }
        $scope.closeModal = function() {
          $scope.modal.hide();
        };
     //Remove modal
     $scope.$on('$destroy', function () {
      $scope.modal.remove();
    });

     //Set $scope.map to null
     $scope.$on('modal.hidden', function () {
       $scope.$on('$destroy', function () {
        $scope.map = null;
      });
     });
     $scope.event.newname = $scope.event.name;
     $scope.event.newdescription = $scope.event.description;
     $scope.event.newstart_date = $scope.event.start_date;
     $scope.event.newend_date = $scope.event.end_date;
     $scope.event.newstart_time = $scope.event.start_time;
     $scope.event.newend_time= $scope.event.end_time;

     //removing image of the event (added by kate)
     $rootScope.ImageFilename = $scope.event.bitmap;
     $scope.removePic = function(){      
      $rootScope.ImageFilename = null;
      var hidePreview = document.getElementById('preview');
      if(hidePreview.style.display==='none'){
       hidePreview.style.display = 'block';
     } else {
      hidePreview.style.display = 'none';
    }
  }
     //added by Renz
     // no reloading is needed to show the details of newly created events

     $scope.UpdateItem = function(event){

      var date = new Date();
      var parts = date.toString().split(" ");
      var months = {Jan: "01",Feb: "02",Mar: "03",Apr: "04",May: "05",Jun: "06",Jul: "07",Aug: "08",Sep: "09",Oct: "10",Nov: "11",Dec: "12"};
      var todayDate =  parts[3]+"/"+months[parts[1]]+"/"+parts[2];
      var todayDateCompare = Date.parse(todayDate);
      var startDate = document.getElementById("datestart").value;
      var startDateCompare = Date.parse(startDate);
      var endDate = document.getElementById("dateend").value;
      var endDateCompare = Date.parse(endDate);


      if((startDate > endDate) || (startDate < todayDate)){
        var alertPopup = $ionicPopup.confirm({
          template: 'Invalid Date/Time'
        });
      }
      else{
        var confirmPopup = $ionicPopup.confirm({
         title: 'Update',
         template: 'Are you sure you want to update this event?'

       });
        confirmPopup.then(function(res) {
          if(res) {     
         //$state.transitionTo($state.current, angular.copy($stateParams), { reload: true, inherit: true, notify: true });
         var preBitmap = event.bitmap;
         console.log("preBitmap: "+preBitmap); 
         event.bitmap = $rootScope.ImageFilename;
         console.log("bitmap: "+event.bitmap); 
         Events.alterevent(event,preBitmap,event.bitmap)
         event.name = event.newname;
         event.location = $scope.event.location;
         event.description = event.newdescription;
         $state.go('menu.events-details', {id: event.id}, {reload: true, inherit: false, notify: true});
       }
       else{
        console.log('Cancelled');
      }
    });

      } 
    } 
  })

.controller('CreateEventsCtrl', function($rootScope,$ionicNavBarDelegate,$scope,Events, $stateParams, $timeout,$state,$ionicPopup, $ionicModal,$compile,$rootScope,AuthService)  {


  $rootScope.askConfirm = function() {
    var confirmPopup = $ionicPopup.confirm({
      title: 'Go Back',
      template: 'You are going back to the previous screen.'

    });

    confirmPopup.then(function(res) {
      if(res) { 
        if($rootScope.isNative){
          location.href='menu-events';
        }else{
          $ionicNavBarDelegate.back();
        } 
      }
    });
  }


  var location = "";
  var address = "";
              var marker = new google.maps.Marker({}); // initialize event marker
              var count = 0; // set counter to initialized the map data only once
              // method for opening the modal
              console.log(count);
              $ionicModal.fromTemplateUrl('templates/map1.html', {
                scope: $scope,
                animation: 'slide-in-up'
              }).then(function(modal) {
                $scope.modal = modal;
              });
              // load the map and display the location
              $scope.openModal = function() {
                  $scope.modal.show(); // shows the modal
                  // if the modal is open for the first time
                  if (count == 0) {
                    var loc;
                    var map = new google.maps.Map(document.getElementById('map_canvas'), {
                      zoom: 5,
                      noClear: true,
                      center: { lat: 12, lng: 120 },
                      mapTypeId: 'roadmap',
                      disableDefaultUI: true
                    });
                      //Create the search box and link it to the UI element.
                      var input = document.getElementById('pac');
                      var searchBox = new google.maps.places.SearchBox(input);
                      map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                      // Bias the SearchBox results towards current map's viewport.
                      map.addListener('bounds_changed', function() {
                        searchBox.setBounds(map.getBounds());
                      });

                      // Listen for the event fired when the user selects a prediction and retrieve
                      // more details for that place.
                      searchBox.addListener('places_changed', function() {
                          marker.setMap(null); // clear out the previous marker
                          var places = searchBox.getPlaces();
                          if (places.length == 0) {
                            return;
                          }
                          // For each place, get the icon, name and location.
                          var bounds = new google.maps.LatLngBounds();
                          places.forEach(function(place) {
                            if (!place.geometry) {
                             console.log("Returned place contains no geometry");
                             return;
                           }
                           loc = place.geometry.location;
                           if (place.geometry.viewport) {
                                 // Only geocodes have viewport.
                                 bounds.union(place.geometry.viewport);
                               } else {
                                 bounds.extend(place.geometry.location);
                               }
                             });
                          map.fitBounds(bounds);
                          address = document.getElementById("pac").value;
                          // $scope.geocodeLatLng(geocoder,{lat: bounds.f.f, lng: bounds.b.b});
                          console.log(address);
                          contentString = "<div><h4>" + address + "</h4><br><button class='button button-clear button-positive' ng-click='setLocation()'>Set as event location</button></div>";
                          var compiled = $compile(contentString)($scope); //complile the content
                          infowindow = new google.maps.InfoWindow({
                            content: compiled[0]
                          });
                          marker = new google.maps.Marker({
                            position: loc,
                            map: map,
                            draggable: true,
                            zoom: 5,
                            title: address
                          });
                          infowindow.open(map, marker);
                          marker.addListener('click', function() {
                            infowindow.open(map, marker);
                          });

                        });
                      $scope.setLocation = function() {
                        $scope.address = address;
                        location = marker.getPosition().lat().toString() + "," + marker.getPosition().lng().toString();
                        console.log(location);
                        $scope.closeModal();
                      }
                    }
                  count = count + 1; // increment the modal if its open once
                };
      // close the modal
      $scope.closeModal = function() {
        $scope.modal.hide();
      };
            //Remove modal
            $scope.$on('$destroy', function () {
              $scope.modal.remove();
            });

              //Set $scope.map to null
              $scope.$on('modal.hidden', function () {
                $scope.$on('$destroy', function () {
                  $scope.map = null;
                });
              });
              $scope.AddItem = function(event){
            //   console.log(event);
          // addr = window.localStorage.getItem("address");
          // location = window.localStorage.getItem("location");
          var timeNow = new Date();
          var minNow = parseInt(timeNow.getMinutes(),10);
          var hrNow = timeNow.getHours()*60;
          var currTime = (parseInt(hrNow + minNow));
          var startTime = document.getElementById("timestart").value;
          var time = startTime.split(':');
          var startTimeCompare = (+time[0]) * 60 + (+time[1]);

          var date = new Date();
          var parts = date.toString().split(" ");
          var months = {Jan: "01",Feb: "02",Mar: "03",Apr: "04",May: "05",Jun: "06",Jul: "07",Aug: "08",Sep: "09",Oct: "10",Nov: "11",Dec: "12"};
          var todayDate =  parts[3]+"/"+months[parts[1]]+"/"+parts[2];
          var todayDateCompare = Date.parse(todayDate);
          var startDate = document.getElementById("datestart").value;
          var startDateCompare = Date.parse(startDate);
          console.log('TODAY DATE: '+todayDateCompare);
          console.log('START DATE: '+startDateCompare);



          if((currTime >= startTimeCompare)&&(todayDateCompare == startDateCompare )){
            var alertPopup = $ionicPopup.confirm({
              template: 'Invalid Time'
            });
          }
          else{
            var confirmPopup = $ionicPopup.confirm({
              title: 'Create',
              template: 'Are you sure you want to create this event?'

            });

            confirmPopup.then(function(res) {
              if(res) {
                var array1 = {
                  name:event.name,
                  start_date:event.start_date,
                  end_date:event.end_date,
                  start_time:event.start_time,
                  end_time:event.end_time,
                  description:event.description,
                  location:location,
                  address:address,
                  bitmap:$rootScope.ImageFilename,
                  created_by: AuthService.userID()
                }
                Events.addevent(array1)
                $state.transitionTo($state.current, angular.copy($stateParams), { reload: true, inherit: true, notify: true });
                $state.go('menu.events', { reload: true, inherit: true, notify: true });
              }
            });
          }
        }

})  //added by Mark and Renz
.controller('ActivityCtrl', function ($scope, AuthService, $http,BASE_URL,$state){

  $scope.moredata = false;
  $scope.logs = [];

  $scope.counter = 0;
  $scope.skip = 0;
  $http.get(BASE_URL.url+'activities?filter[order]=date%20DESC&filter[limit]=5&filter[skip]='+$scope.skip+'&access_token='+AuthService.accessToken()).success(function(data) {
    angular.forEach(data, function(value, key) {
      $scope.logs.push(value);
    })
  })
  
  $scope.loadMoreData=function(){

    $scope.counter++;
    $scope.skip = $scope.counter*5;
    $http.get(BASE_URL.url+'activities?filter[order]=date%20DESC&filter[limit]=5&filter[skip]='+$scope.skip+'&access_token='+AuthService.accessToken()).success(function(data) {
      angular.forEach(data, function(value, key) {
        $scope.logs.push(value);
      })
    })  
    $scope.items.push({id: $scope.items.length});
    if($scope.items.length==100)
    {
      $scope.moredata=true;
    }
    $scope.$broadcast('scroll.infiniteScrollComplete');
  };

  $scope.items=[];
})

.controller('loginCtrl', function($scope,$state, $ionicPopup,$stateParams, $timeout, AuthService, AUTH_EVENTS) {
  $scope.data = {};
  $scope.login = function(data) {
    console.log(data);
        //var details = {'email': data.username, 'password': data.password};
        // var encpass = Base64.encode(data.password);
        // console.log("LOGIN user: n/"  + data.username + " - PW: n/" + data.password + " Encrypt: "+ encpass);
        // var details = {'email': data.username, 'password': data.password};
        // console.log("LOGIN user: " + data.username + " - PW: " + data.password);

        AuthService.login(data.email, data.password).then(function(authenticated) {
        // $state.transitionTo($state.current, $stateParams, { reload: true, inherit: false, notify: true });
        //  console.log(AuthService.password());
         location.reload(); 
         $state.go('menu.invited-events');
        //  location.reload();
         $scope.setCurrentUsername(data.email);
       }, function(err) {
        var alertPopup = $ionicPopup.alert({
         title: 'Login failed!',
         template: 'Please check your credentials!'
       });
      });
      };
    })

.controller('MapController', function($scope,Events, $stateParams, $timeout,$state,$ionicPopup, $ionicModal,$compile)  {
})
.controller('CheckInCtrl',function($scope,Events,$stateParams,$ionicPopup, AuthService, Users, EventUsers,$cordovaGeolocation){
  var lat = [];
  var lng = [];
  $scope.CheckIn = function(){    
    var locationOfEvent = $scope.event.location;
    //event location in terms of latitude and longitude
    var eventLatlong = {
      lat: parseFloat((locationOfEvent.split(',')[0])),
      lng: parseFloat((locationOfEvent.split(',')[1]))
    };
    console.log("event location: "+eventLatlong);
    
    var posOptions = {timeout: 10000};
    $cordovaGeolocation
    .getCurrentPosition(posOptions)
    
    .then(function (position) {
        lat["0"] = position.coords.latitude;
        lng["1"] = position.coords.longitude;
      }, function(err) {
        console.log(err)
      });
    //current location of user is saved in myLocation
    var myLocation = {
      lat: parseFloat(lat["0"]),
      lng: parseFloat(lng["1"])
    };
    console.log("my location: "+myLocation);
    
    //calculate distance from current location to event location; return value is in meters
    var distance = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(eventLatlong), new google.maps.LatLng(myLocation));
    console.log("distance: "+distance);
    var eventUser = EventUsers.getEventUser($scope.event.id, AuthService.userID(),distance);
  };
})
